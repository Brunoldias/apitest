﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Test.Controllers
{

    [ApiController]
    [Route("[controller]")]
    public class EchoController : ControllerBase
    {
        public IActionResult Index()
        {
            return Ok("It's working! ;)");
        }
    }
}
